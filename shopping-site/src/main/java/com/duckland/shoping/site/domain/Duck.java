package com.duckland.shoping.site.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface Duck {
    boolean camFly();

    boolean canSwim();

    Integer fly(String timeInSeconds) throws Exception;

    Integer swim(Optional<List<Duck>> ducks);

    interface DuckTracking {
        TrackingData[] localisations();

        public class TrackingData {
            String id;
            Date date;
            List<WildDuck> neighbours;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public Date getDate() {
                return date;
            }

            public void setDate(Date date) {
                this.date = date;
            }

            public List<? extends Duck> getNeighbours() {
                return neighbours;
            }

            public void setNeighbours(List<WildDuck> neighbours) {
                this.neighbours = neighbours;
            }
        }
    }

    public class WildDuck implements Duck, DuckTracking {
        private String name;
        Integer numberKmPerDay = 35;
        public static final SimpleDateFormat sdf = new SimpleDateFormat("ss");
        protected List<TrackingData> trackingData;
        private List<WildDuck> neighbours = new ArrayList<>();

        WildDuck(String uniqueName) {
            this.name = uniqueName;
        }

        @Override
        public boolean camFly() {
            return false;
        }

        @Override
        public TrackingData[] localisations() {
            return trackingData.toArray(new TrackingData[] {});
        }

        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public Integer fly(String timeInSeconds) throws Exception {
            try {
                Date success = sdf.parse(timeInSeconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // le parsing de la date à réussi donc on peut l'utiliser
            numberKmPerDay -= (Integer.valueOf(timeInSeconds) * 1);
            if (numberKmPerDay < 0) {
                throw new Exception("The duck is tired !");
            }
            return (Integer.valueOf(timeInSeconds) * 1);
        }

        @Override
        public Integer swim(Optional<List<Duck>> ducks) {
            this.neighbours = ducks.get().stream()
                    .filter(duck -> duck.canSwim())
                    .map(duck -> (WildDuck) duck)
                    .collect(Collectors.toList());

            TrackingData trackingData = new TrackingData();
            trackingData.setDate(new Date());
            trackingData.setId(this.name);
            trackingData.setNeighbours(this.neighbours);
            this.trackingData.add(trackingData);

            if (!canSwim()) {
                return null;
            }
            // a duck never tired when it swim !
            ++numberKmPerDay;

            return 8 * 5;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            WildDuck wildDuck = (WildDuck) o;

            return new EqualsBuilder()
                    .append(numberKmPerDay, wildDuck.numberKmPerDay)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(name)
                    .toHashCode();
        }
    }
}
